#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

# Datos de la aplicacion y maquina
APP_NAME = "personalControl"
APP_STRTITLE = "Control de Personal"
APP_VERSION = 1.0
CFG_TERMINALID=None
CFG_TERMINALNAME=None

# Ficheros apuntadores de los serialsUSB
FILE_RFIDMOTOR=None
FILE_IDREADER=None
FILE_CAMERA=None

# PATTERNS FOR DEVS
FILE_RFIDMOTOR_PATTERN = "/dev/serial/by-id/usb-Cega_Electronica_UART_RFIDMotor*"
FILE_CAMERA_PATTERN = "/dev/video*"
FILE_IDREADER_PATTERN = "/dev/serial/by-id/usb-Honeywell_Imaging___Mobility_1400g_*"

# CONFIGS PARA IDENTIFICADORES
PDFARGDELIMITER = '@'				# Separador en PDFs417
STRUCT_DATAPDF1 = [1,4,5,7,8]			# Posicion dentro de array
STRUCT_DATAPDF2 = [4,1,2,6,3]			# Posicion dentro de array

# DEBUGGING AND TESTING
IAMDEBUGGING = True
IAMTESTING = False				# !!!!!!!! TRUE EVITA EL ENVIO DE DATOS FINALES <> EN PRODUCCION PONER EN FALSE

# MISC
CONST_NOCREDENTIALS = "NINGUNATAR"

# TIMEOUTS
TIMEOUT_PHTAKEN = 10        			# Segundos para tomar foto
TIMEOUT_RFID = 10
TIMEOUT_BACKLIGHT_TURNOFF = 1200				# Segundos
TIMEOUT_BACKLIGHT_STANDBY = 600                         # Segundos

# WEBCAM
WEBCAM_WIDTH = 160
WEBCAM_HEIGHT = 120
WEBCAM_MESSAGE = ""				# DEPRECATED

# Directorios
DIRCORE = os.path.dirname(os.path.abspath(__file__))
DIRROOT = os.path.dirname(DIRCORE)

# XML
CONSTXML_ROOT="root"
CONSTXML_IDNUSUARIO="asegurado_id"
CONSTXML_NOMYAPE="nombre"
CONSTXML_FECHANAC="fecha_nacimiento"
CONSTXML_DNI="documento_numero"
CONSTXML_RFID="numero"
CONSTXML_TERMINALID="beneficiario_id"
CONSTXML_ACTIVIDADID="actividad_id"
CONSTXML_IMAGEB64="foto"
CONSTXML_USERSTATUS="_userStatus"
CONSTXML_USERSTATUS_EARLY="1"                   # RECIEN CREADO
CONSTXML_USERSTATUS_BLACKLIST="0"               # EN LISTA NEGRA
CONSTXML_USERSTATUS_MODIFIED="2"                # MODIFICADO

# SERVER EXTERNAL
CONSTHOST_URLADDUSER="http://www.taker.com.ar/taker_ap_barrios/autogestion/trabajador_agregar"
CONSTHOST_URLUPDUSER="http://www.taker.com.ar/taker_ap_barrios/autogestion/carnet_cambiar"
CONSTHOST_URLCHECKDNI="http://www.taker.com.ar/taker_ap_barrios/autogestion/aprobar_tarjeta_by_dni"
CONSTHOST_URLCHECKIDN="http://www.taker.com.ar/taker_ap_barrios/autogestion/aprobar_tarjeta_by_asegurado_id"
CONSTHOST_STATICPING="8.8.8.8"
#CONSTHOST_URL="http://192.168.7.1/htdocs/pruebasss/pythonpost.php"

