#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
import base64
from PyQt4 import QtCore
from PyQt4.QtCore import QByteArray, QBuffer, QIODevice
from configAPP import *
from global_func import *
import Image
from mainXML import *
from tempfile import mkstemp
from shutil import move
from os import remove, close

# Clase de tipo Usuario con sus atributos y metodos
class Usuario(object):
  idNUsuario = ""
  dniUsuario = ""
  rfidUsuario = ""
  nombresUsuario = u''
  apellidosUsuario = u''
  fechaNacUsuario = ""
  imgPhoto = None

  def __init__(self):
    pass

  def setRFID(self,rfid):
    self.rfidUsuario = rfid

  def setDNIData(self, dni, apellidos, nombres, fechanac):
    self.dniUsuario = dni
    self.nombresUsuario = nombres
    self.apellidosUsuario = apellidos
    self.fechaNacUsuario = datetime.strptime(fechanac, "%d/%m/%Y").strftime("%Y-%m-%d")

  def setIDNData(self, idnro):
    self.idNUsuario = idnro

  def setImgPhoto(self, imgPhoto):
    self.imgPhoto = imgPhoto

  def getImageHardcode(self):
    byteArray = QtCore.QByteArray()
    bufferByteArray = QtCore.QBuffer(byteArray)
    bufferByteArray.open(QtCore.QIODevice.WriteOnly)
    self.imgPhoto.save(bufferByteArray, 'JPEG')
    return byteArray.toBase64().data()

  def toString(self):
    return (u" $ DatosU: %s, %s, %s, %s." % (self.dniUsuario, self.nomyapeUsuario, self.fechaNacUsuario, self.rfidUsuario ))

class barcodeC39(object):

  idNUser = None

  def __init__(self):
    pass

  def setData(self, nro):
    self.idNUser = nro.strip()

class barcodePDF(object):

  pdfDNI = ""
  pdfApellidos = u''
  pdfNombres = u''
  pdfFechaNac = ""

  def __init__(self):
    pass

  def setData(self, dni, apellidos, nombres, fechanac):
    self.pdfDNI = dni.strip()
    self.pdfApellidos = apellidos.strip()
    self.pdfNombres = nombres.strip()
    self.pdfFechaNac = fechanac.strip()

class CONFIGDEVS(object):

  fileRFIDMOTOR = None
  fileCAMERA = None
  fileIDREADER = None

  def __init__(self):
    pass

  def changeCFGfile(self, cleanDEVs=None):
    file_path=os.path.dirname(os.path.abspath(__file__))+"/configAPP.py"

    patternRFIDMOTOR="FILE_RFIDMOTOR="
    if self.fileRFIDMOTOR:
      if not cleanDEVs:
        substRFIDMOTOR="FILE_RFIDMOTOR=\""+self.fileRFIDMOTOR+"\""
      else:
        substRFIDMOTOR="FILE_RFIDMOTOR=None"
    else:
      substRFIDMOTOR="FILE_RFIDMOTOR=None"

    patternCAMERA="FILE_CAMERA="
    if self.fileCAMERA:
      if not cleanDEVs:
        substCAMERA="FILE_CAMERA=\""+self.fileCAMERA+"\""
      else:
        substCAMERA="FILE_CAMERA=None"
    else:
      substCAMERA="FILE_CAMERA=None"

    patternIDREADER="FILE_IDREADER="
    if self.fileIDREADER:
      if not cleanDEVs:
        substIDREADER="FILE_IDREADER=\""+self.fileIDREADER+"\""
      else:
        substIDREADER="FILE_IDREADER=None"
    else:
      substIDREADER="FILE_IDREADER=None"

    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
      with open(file_path) as old_file:
        for line in old_file:
          if patternRFIDMOTOR in line:
            new_file.write(substRFIDMOTOR+"\n")
          elif patternCAMERA in line:
            new_file.write(substCAMERA+"\n")
          elif patternIDREADER in line:
            new_file.write(substIDREADER+"\n")
          else:
            new_file.write(line)
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

  def getRFIDMOTOR(self):
    return self.fileRFIDMOTOR

  def getCAMERA(self):
    return self.fileCAMERA

  def getIDREADER(self):
    return self.fileIDREADER

  def getNotDetected(self):
    arrNotDetected = []
    if not self.fileRFIDMOTOR:
      arrNotDetected.append("- RFID/Motores")
    if not self.fileCAMERA:
      arrNotDetected.append("- Cámara")
    if not self.fileIDREADER:
      arrNotDetected.append("- Lector de identificación")
    return arrNotDetected

  def detectDevs(self):
    global FILE_RFIDMOTOR_PATTERN
    global FILE_CAMERA_PATTERN
    global FILE_IDREADER_PATTERN

    printDEBUG("Detectando dispositivos")
    arrRFIDMOTOR = glob.glob(FILE_RFIDMOTOR_PATTERN)
    arrCAMERA = glob.glob(FILE_CAMERA_PATTERN) 
    arrIDREADER = glob.glob(FILE_IDREADER_PATTERN) 

    if arrRFIDMOTOR:
      self.fileRFIDMOTOR = arrRFIDMOTOR[0]
    if arrCAMERA:
      self.fileCAMERA = arrCAMERA[0]
    if arrIDREADER:
      self.fileIDREADER = arrIDREADER[0]

    if self.fileRFIDMOTOR:
      printDEBUG( "Configurando '%s' como controlador de 'Lector de tarjetas'" % ( self.fileRFIDMOTOR ) )
    else:
      printDEBUG( "No se detecto controladora de 'Lector de tarjetas'" )

    if self.fileCAMERA:
      printDEBUG( "Configurando '%s' como controlador de 'Camara'" % ( self.fileCAMERA ) )
    else:
      printDEBUG( "No se detecto controladora de 'Camara'" )

    if self.fileIDREADER:
      printDEBUG( "Configurando '%s' como controlador de 'RFID y Motor'" % ( self.fileIDREADER ) )
    else:
      printDEBUG( "No se detecto controladora de 'RFID y Motor'" )

    self.changeCFGfile()

class CONFIGAPP(object):

  terminalID = ""
  terminalNAME = ""
  factoryReset = False
  softwareReset = False

  def __init__(self, arrCONFIGS):

    global CFG_TERMINALID
    global CFG_TERMINALNAME

    printDEBUG("Configurando terminal...")
    if ( ( len(arrCONFIGS) == 1 ) and ( "factoryReset" in arrCONFIGS[0] ) ):
      self.factoryReset = True
      printDEBUG("Se encontro 'Reinicio de fabrica'")
    elif ( ( len(arrCONFIGS) == 1 ) and ( "softwareReset" in arrCONFIGS[0] ) ):
      self.softwareReset = True
      printDEBUG("Se encontro 'Reinicio de software'")
    else:
      for xCONFIG in arrCONFIGS:
        if "id=" in xCONFIG:
          self.terminalID = xCONFIG.replace( "id=", "" )
        if "terminal=" in xCONFIG:
          self.terminalNAME = xCONFIG.replace( "terminal=" , "" )
      printDEBUG("PERSISTIENDO NOMBRE: " + self.terminalNAME )
      printDEBUG("PERSISTIENDO ID: " + self.terminalID )

  def getID(self):
    return self.terminalID

  def getNAME(self):
    return self.terminalNAME

  def getResetFactory(self):
    return self.factoryReset

  def getResetSoftware(self):
    return self.softwareReset

  def changeCFGfile(self):
    file_path=os.path.dirname(os.path.abspath(__file__))+"/configAPP.py"

    patternID="CFG_TERMINALID="
    substID="CFG_TERMINALID="+self.terminalID

    patternNAME="CFG_TERMINALNAME="
    substNAME="CFG_TERMINALNAME=\""+self.terminalNAME+"\""

    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
      with open(file_path) as old_file:
        for line in old_file:
          if patternID in line:
            new_file.write(substID+"\n")
          elif patternNAME in line:
            new_file.write(substNAME+"\n")
          else:
            new_file.write(line)
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

  def factoryCFGfile(self):
    file_path=os.path.dirname(os.path.abspath(__file__))+"/configAPP.py"

    patternID="CFG_TERMINALID="
    substID="CFG_TERMINALID=None"

    patternNAME="CFG_TERMINALNAME="
    substNAME="CFG_TERMINALNAME=None"

    patternRFIDMOTOR="FILE_RFIDMOTOR="
    substRFIDMOTOR="FILE_RFIDMOTOR=None"

    patternCAMERA="FILE_CAMERA="
    substCAMERA="FILE_CAMERA=None"

    patternIDREADER="FILE_IDREADER="
    substIDREADER="FILE_IDREADER=None"

    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
      with open(file_path) as old_file:
        for line in old_file:
          if patternID in line:
            new_file.write(substID+"\n")
          elif patternNAME in line:
            new_file.write(substNAME+"\n")
          elif patternRFIDMOTOR in line:
            new_file.write(substRFIDMOTOR+"\n")
          elif patternCAMERA in line:
            new_file.write(substCAMERA+"\n")
          elif patternIDREADER in line:
            new_file.write(substIDREADER+"\n")
          else:
            new_file.write(line)
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

