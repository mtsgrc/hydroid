#!/usr/bin/env python
# -*- coding: utf-8 -*-

from class_main import *
from configAPP import *
from global_func import *
import re


class scannerBARCODE(object):

  strPlain = None

  def __init__(self, textToSet):
    printDEBUG("Text to analyze: '%s'" % ( textToSet ))
    self.strPlain = textToSet

  def detectTypeBarcode(self):

    try:
      if re.compile('^@\d{7,} *@[A-Z]{1}@\d{1}').findall(self.strPlain.strip()):
        return "pdfarg1"

      if re.compile('^\d{11}@[A-Z ]*@[A-Z ]*@[M|F]{1}@\d{7,}@').findall(self.strPlain.strip()):
        return "pdfarg2"

      if re.compile('^\d{11}@[A-Z ]*@[A-Z ]*@[M|F]{1}@[M|F]{1}\d{7,}@').findall(self.strPlain.strip()):
        return "pdfarg3"

      if re.compile('^\d+$').findall(self.strPlain.strip()):
        return "oldcarnet"

      if re.compile('^\d{8}[0-9|K]{1}\d{9} ').findall(self.strPlain.strip()):
        return "pdfchl1"

      if re.compile('^https:\/\/').findall(self.strPlain.strip()):
        return "pdfchl2"

      if re.compile('^CONFIG{').findall(self.strPlain.strip()):
        return "configApp"

      if re.compile('^TESTAPP{').findall(self.strPlain.strip()):
        return "testApp"

      return "unknown"

    except:
      return "unknown"

  # Creamos objeto con datos de PDF para adjuntar luego ala usuario, sino devolvemos None
  def getUserObjectFromString(self):

    objPDFdata = None

    try:

      if not self.strPlain:
        return None

      typeBarcode = self.detectTypeBarcode()
      printDEBUG( "Deteccion del tipo de identificacion: %s" % ( typeBarcode ) )

      if typeBarcode == "unknown":
        return None
      elif typeBarcode == "testApp":
        strTESTAPP = self.strPlain[8:-1]
        if strTESTAPP == "on":
          return "testAppOn"
        else:
          return "testAppOff"
      elif typeBarcode == "configApp":                                                            # CONFIGURACIONES
        strCONFIG = self.strPlain[7:-1]
        arrCONFIG = strCONFIG.split(";")
        objCONFIG = CONFIGAPP(arrCONFIG)
        return objCONFIG


      if "pdfarg" in typeBarcode:
        arrPDF = self.strPlain.split(PDFARGDELIMITER)
        objPDFARG = barcodePDF()

        if typeBarcode == "pdfarg1":
          objPDFARG.setData( arrPDF[1], arrPDF[4], arrPDF[5], arrPDF[7] )
        elif typeBarcode == "pdfarg2":
          objPDFARG.setData( arrPDF[4], arrPDF[1], arrPDF[2], arrPDF[6] )
        elif typeBarcode == "dniarg3":
          objPDFARG.setData( arrPDF[4][1:], arrPDF[1], arrPDF[2], arrPDF[6] )
        else:
          pass

        return objPDFARG

      elif typeBarcode == "oldcarnet":
        objCODE39 = barcodeC39()
        objCODE39.setData ( self.strPlain )
        return objCODE39

      elif typeBarcode == "pdfchl1" or typeBarcode == "pdfchl2":                          # Aca agregar para Chile
        return None

      else:
        return None

    except:
      return None

