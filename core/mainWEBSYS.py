#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import shlex
import subprocess
from configAPP import *
from global_func import *
from mainXML import *
from urlparse import urlparse

RETXML_IDNNOEXIST = "asegurado_id no existe"
RETXML_UPENDIENTE = "asegurado pendiente"
RETXML_MAXCARDS = "cantidad de tarjetas superado"
RETXML_GIVECARD = "true"

class WebSysSender(object):

  userDNI = {}
  userIDN = {}

  def __init__(self):
    pass

  def setDNI(self, xmlData):
    dniXML = etree.fromstring(xmlData)
    self.userDNI['nombre'] = unicode(dniXML.find(CONSTXML_NOMYAPE).text, "utf-8")
    self.userDNI['documento_numero'] = int(dniXML.find(CONSTXML_DNI).text)
    self.userDNI['numero'] = dniXML.find(CONSTXML_RFID).text
    self.userDNI['fecha_nacimiento'] = dniXML.find(CONSTXML_FECHANAC).text
    self.userDNI['beneficiario_id'] = int(CFG_TERMINALID)
    self.userDNI['foto'] = unicode(dniXML.find(CONSTXML_IMAGEB64).text, "utf-8")

  def setIDN(self, xmlData):
    idnXML = etree.fromstring(xmlData)
    self.userIDN['asegurado_id'] = idnXML.find(CONSTXML_IDNUSUARIO).text
    self.userIDN['numero'] = idnXML.find(CONSTXML_RFID).text
    self.userIDN['beneficiario_id'] = int(CFG_TERMINALID)
    self.userIDN['foto'] = unicode(idnXML.find(CONSTXML_IMAGEB64).text, "utf-8")

  def sendDNI(self):
    try:
      printDEBUG( "Sending POST data to \"%s\" " % (CONSTHOST_URLADDUSER) )
      printDEBUG( ">>>Data to send: \n %s " % ( str(self.userDNI) ) )
      retRequest = requests.post(CONSTHOST_URLADDUSER, data=self.userDNI, timeout=3)
      printDEBUG(retRequest.text.strip())
      return retRequest.text.strip()
    except Exception as exc:
      printDEBUG("Error al enviar datos al servidor")
      printDEBUG(str(exc))
      return None

  def sendIDN(self):
    try:
      printDEBUG( "Sending POST data to \"%s\" " % (CONSTHOST_URLUPDUSER) )
      printDEBUG( ">>>Data to send: \n %s " % ( str( self.userIDN ) ) )
      retRequest = requests.post(CONSTHOST_URLUPDUSER, data=self.userIDN, timeout=3)
      printDEBUG(retRequest.text.strip())
      return retRequest.text.strip()
    except Exception as exc:
      printDEBUG("Error al enviar datos al servidor")
      printDEBUG(str(exc))
      return None

  def checkDNI(self, dni):
    try:
      postDNI = {}
      postDNI['documento_numero'] = dni
      postDNI['beneficiario_id'] = int(CFG_TERMINALID)
      retRequest = requests.post(CONSTHOST_URLCHECKDNI, data=postDNI, timeout=10)
      printDEBUG("Respuesta del servidor al checkear documento_numero(%s): %s" % ( dni , retRequest.text.strip() ))
      return retRequest.text.strip()
    except Exception as exc:
      printDEBUG("Error al checkear datos al servidor")
      printDEBUG(str(exc))
      return None

  def checkIDN(self, idn):
    try:
      postIDN = {}
      postIDN['asegurado_id'] = idn
      postIDN['beneficiario_id'] = int(CFG_TERMINALID)
      retRequest = requests.post(CONSTHOST_URLCHECKIDN, data=postIDN, timeout=10)
      printDEBUG("Respuesta del servidor al checkear asegurado_id(%s): %s" % ( idn , retRequest.text.strip() ))
      return retRequest.text.strip()
    except Exception as exc:
      printDEBUG("Error al checkear datos al servidor")
      printDEBUG(str(exc))
      return None
