#!/usr/bin/env python
# -*- coding: utf-8 -*-

import PyQt4
from PyQt4 import QtCore, QtGui
import time
import sys
import os
import traceback
import Image
import cv
import sys

from PyQt4.QtCore import QPoint, QTimer, QMargins
from PyQt4.QtGui import QApplication, QImage, QPainter, QWidget
from global_func import *
from configAPP import *
from threading import Timer


class camCapturer(object):

  cam = None
  snapshot = None
  refWidget = None

  def __init__(self, refWidget):
    self.refWidget = refWidget
    self.webcam = cv.CreateCameraCapture(-1)
    cv.SetCaptureProperty(self.webcam,cv.CV_CAP_PROP_FRAME_WIDTH,WEBCAM_WIDTH)
    cv.SetCaptureProperty(self.webcam,cv.CV_CAP_PROP_FRAME_HEIGHT,WEBCAM_HEIGHT)
    cv.WaitKey(10)
    cv.DestroyAllWindows()

  def showFrame(self,lblWebcam):
    ipl_image = cv.QueryFrame(self.webcam)
    data = ipl_image.tostring()
    self.snapshot = QtGui.QImage(data, ipl_image.width, ipl_image.height, ipl_image.channels * ipl_image.width, QtGui.QImage.Format_RGB888)
    pixmap = QtGui.QPixmap()
    self.snapshot = self.snapshot.rgbSwapped()
    pixmap.convertFromImage(self.snapshot)
    lblWebcam.setPixmap(pixmap)

  def streamAndCapture(self):

    self.streaming = None
    def capturePicture():
        global streaming
        self.streaming = False

    try:

      # Creamos Qlabel donde mostraremos streaming
      stepCAMlblWebcam = QtGui.QLabel(self.refWidget.ui.step2_streamBox)
      stepCAMlblWebcam.setGeometry(QtCore.QRect(10,10, WEBCAM_WIDTH,WEBCAM_HEIGHT))
      stepCAMlblWebcam.show()

      # Marcamos tiempo actual para calcular tiempo restante
      initTime = time.time()
      timerOut = Timer(TIMEOUT_PHTAKEN, capturePicture)
      timerOut.start()

      # Indicamos inicio de streaming
      self.streaming = True
      while self.streaming == True:

        # Refresh de pantalla
        self.showFrame(stepCAMlblWebcam)

        # Calculamos tiempo restante
        timeleft = TIMEOUT_PHTAKEN - int(time.time() - initTime)
        time.sleep(0.02)

        # Mostramos tiempo si a partir de 5 segundos restantes
        if timeleft <= 5 and timeleft >= 0:
          self.refWidget.ui.step2_timeleft.setText(str(timeleft))

        # Refresh
        self.refWidget.repaint()                                      # Refresh GUI
        QtGui.QApplication.processEvents()                            # Necesary for loops

      # Del
      del(self.webcam)

      # Regresh GUI
      self.refWidget.repaint()
      QtGui.QApplication.processEvents()

      # Esperamos un segundo mas
      time.sleep(1)

      # Ocultamos y cerramos objetos
      self.refWidget.ui.step2_timeleft.hide()

      timerOut.cancel()
      time.sleep(3)
      return True

    except Exception as exc:
      printOUT("No se pudo iniciar la camara. " + str(exc))
      traceback.print_exc(file=sys.stdout)
      snapshot = None
      return False
