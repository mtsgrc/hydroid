#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
from class_main import *
import os, sys
import os.path

class xmlData(object):

  xmlUser = None

  def __init__(self):
    pass

  def setXMLfromUSEROBJ(self, objUsuario):
    self.xmlUser = etree.Element(CONSTXML_ROOT)

    xmlData_idNUsuario = etree.Element(CONSTXML_IDNUSUARIO)
    xmlData_nomyape = etree.Element(CONSTXML_NOMYAPE)
    xmlData_dni = etree.Element(CONSTXML_DNI)
    xmlData_rfid = etree.Element(CONSTXML_RFID)
    xmlData_fechaNac = etree.Element(CONSTXML_FECHANAC)
    xmlData_img64 = etree.Element(CONSTXML_IMAGEB64)
    xmlData_statusUser = etree.Element(CONSTXML_USERSTATUS)

    xmlData_idNUsuario.text = objUsuario.idNUsuario
    xmlData_nomyape.text = "%s %s" % (objUsuario.apellidosUsuario.upper(), objUsuario.nombresUsuario.upper())
    xmlData_fechaNac.text = objUsuario.fechaNacUsuario
    xmlData_dni.text = objUsuario.dniUsuario
    xmlData_rfid.text = objUsuario.rfidUsuario
    xmlData_img64.text = objUsuario.getImageHardcode()
    xmlData_statusUser.text = CONSTXML_USERSTATUS_EARLY

    self.xmlUser.append(xmlData_idNUsuario)
    self.xmlUser.append(xmlData_nomyape)
    self.xmlUser.append(xmlData_dni)
    self.xmlUser.append(xmlData_rfid)
    self.xmlUser.append(xmlData_fechaNac)
    self.xmlUser.append(xmlData_img64)
    self.xmlUser.append(xmlData_statusUser)

  def setXMLfromXML(self, xmlObject):
    self.xmlUser = xmlObject

  def getXMLstring(self):
    if self.xmlUser is not None:
      return etree.tostring(self.xmlUser)
    else:
      return "XML vacio"

  def saveXML(self, synced):
    try:

      if self.getValue(CONSTXML_DNI):
        userNamefile = self.getValue(CONSTXML_DNI)
      else:
        userNamefile = self.getValue(CONSTXML_IDNUSUARIO)

      pathFile = DIRCORE + "/bd/_" + userNamefile + ".xml"
      pathFileSynced = DIRCORE + "/bd/" + userNamefile + ".xml"

      if os.path.isfile(pathFile):
        os.remove(pathFile)

      if os.path.isfile(pathFileSynced):
        os.remove(pathFileSynced)

      if synced == True:
        printOUT("Fichero XML a guardar:" + pathFileSynced)
        fileXML = open(pathFileSynced, 'w')
        fileXML.write(etree.tostring(self.xmlUser,pretty_print=True, xml_declaration=True, encoding='utf-8'))
        fileXML.close()

      else:
        printOUT("Fichero XML a guardar:" + pathFile)
        fileXML = open(pathFile, 'w')
        fileXML.write(etree.tostring(self.xmlUser,pretty_print=True, xml_declaration=True, encoding='utf-8'))
        fileXML.close()

    except:
      printOUT("E: No se pudo guardar fichero XML")

  def getValue(self, tag):
    try:
      return self.xmlUser.find(tag).text
    except:
      return ''
