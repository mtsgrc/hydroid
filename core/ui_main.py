#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import pyqtSlot,SIGNAL,SLOT

from userControlWizard import Ui_registerWizard

from mainBARCODE import *
from mainCAMERA import *
from class_main import *

from configAPP import *
from global_func import *

import glob
import serial
import sys, traceback
import time
import threading
import traceback
import os.path
from threading import Timer
from sys import exit
from StringIO import StringIO

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


# Clase principal (Creacion de Ventana)
class MainWizard(QtGui.QStackedWidget):

  tmpUserInput = None

  def __init__(self):
    # Inicializacion de ventana
    super(MainWizard, self).__init__()
    QtGui.QMainWindow.showFullScreen(self)
    self.ui = Ui_registerWizard()
    self.ui.setupUi(self)
    #self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    #self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowStaysOnTopHint)
    self.connect(self.ui.step1_inputuser, SIGNAL("returnPressed()"),self.inputInserted)

  def inputInserted(self):
    self.tmpUserInput = (u"%s" % self.ui.step1_inputuser.text()).encode('utf-8')
    self.ui.step1_inputuser.setText("")

  def goToStep(self,stepText,strTEXT="", timeTEXT=5.0):
    stepToGo = 0
    for auxStep in range(self.count()):
      auxWidget = self.widget(auxStep)
      if auxWidget.objectName() == stepText:
        stepToGo = auxStep
        break

    if stepText == "widget1":
      self.ui.step1_inputuser.setFocus()
      strStepText = "  > Ingreso de Codigo de barras... "
    elif stepText == "widget2":
      strStepText = "  > Captura de foto de usuario... "
    elif stepText == "widget3":
      strStepText = "  > Muestra de datos ingresados... "
    elif stepText == "widget4":
      strStepText = "  > Retiro de nueva credencial... "
    elif stepText == "widgetMSSG":
      strStepText = "  > Muestra de Mensaje: '%s' " % ( _fromUtf8(strTEXT) )
      self.ui.labelMSSG.setText(_fromUtf8(strTEXT))
    else:
      strStepText = ""

    printOUT(strStepText, True)

    self.setCurrentIndex(stepToGo)
    self.repaint()                                      # Refresh de pantalla
    QtGui.QApplication.processEvents()

    if stepText == "widgetMSSG":
      time.sleep(timeTEXT)


# Clase que maneja el paso de Expulsarle una credencial al Usuario
class StepRFID(object):

  serialPORT = None
  actRFID = None

  def __init__(self):
    pass

  def listenRFID(self):
    if os.path.exists(FILE_RFIDMOTOR):
      self.serialPORT = serial.Serial(port=FILE_RFIDMOTOR)
      self.serialPORT.flushOutput()
      self.serialPORT.write("s")
      self.serialPORT.close()

      self.actRFID = getCOMMData(FILE_RFIDMOTOR, TIMEOUT_RFID)
    else:
      self.actRFID = None





# Clase que maneja el paso de Obtener datos del Usuario presente
class StepScannerBarcode(object):
  strDataBarcoder = ""
  objIDScanner = None

  def __init__(self):
    pass

  def waitBarcode(self):
    if os.path.exists(FILE_IDREADER):
      self.strDataBarcoder = getCOMMData(FILE_IDREADER)
    else:
      self.strDataBarcoder = None

  def getUserObject(self):
    try:
      if self.strDataBarcoder:
        objIDScanner = scannerBARCODE(self.strDataBarcoder)
        return objIDScanner.getUserObjectFromString()
    except:
      return None

  def getDataString(self):
    if self.strDataBarcoder:
      return self.strDataBarcoder
    else:
      return "EMPTY SERIAL DATA IN"


# Clase que maneja el paso de Tomar foto al Usuario presente
class StepCAM(object):

  objCAMERA = None
  objImage = None
  refWidget = None

  def __init__(self, refWidget):
    self.objCAMERA = camCapturer(refWidget)

  def streamAndCapture(self):
    try:
      resultCAM = None
      resultCAM = self.objCAMERA.streamAndCapture()
      if resultCAM != None:
        self.objImage = self.objCAMERA.snapshot
      else:
        self.objImage = None
    except Exception as exc:
      print "%s" % (str(exc))
      traceback.print_exc(file=sys.stdout)
      self.objImage = None



# Clase que maneja el paso de Mostrarle los datos al Usuario presente
class StepEND(object):

  def __init__(self):
    pass

  def showDataInWidget(self,widget,userData,snapshot):

    if userData.idNUsuario:
      widget.ui.step3_userData.setText(_fromUtf8("<html><head></head><body><font style='font-size: 14pt;'><b><font style='font-size: 16pt;'>Número</font><br />Identificación</b><br />Asegurado<br />#" + str(userData.idNUsuario) + "</font></body></html>"))
    else:
      widget.ui.step3_userData.setText("<html><head></head><body><font style='font-size: 14pt;'><b><font style='font-size: 16pt;'>" + _fromUtf8(userData.apellidosUsuario) + "</font><br />" + _fromUtf8(userData.nombresUsuario) + "</b><br />" + formatDNI(userData.dniUsuario) + "<br />" + formatDATE(userData.fechaNacUsuario) + "</font></body></html>")

    imgImage=QtGui.QLabel(widget.ui.step3_streamBox)
    imgImage.setGeometry(QtCore.QRect(10,10,WEBCAM_WIDTH,WEBCAM_HEIGHT))
    imgImage.setScaledContents(True)
    pixmap = QtGui.QPixmap()
    pixmap.convertFromImage(snapshot)
    imgImage.setPixmap(pixmap)
    imgImage.show()

    widget.repaint()                                                    # Refresh de pantalla
    QtGui.QApplication.processEvents()
