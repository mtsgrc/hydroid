#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import os, sys, stat, traceback
from datetime import date, datetime, time, timedelta
import glob
import time
import easygui
import shlex
import subprocess
import serial
from serial import SerialException, SerialTimeoutException

from configAPP import *
from Tkinter import Tk
from contextlib import contextmanager
from threading import Timer

WHERE_IAM = os.path.dirname(os.path.abspath(__file__))
DB_DIR = WHERE_IAM + "/bd"
DBIMGS_DIR = DB_DIR + "/imgs"

def printOUT(mssg,newline=True):
  if newline==True:
    appendLOG(u'\n%c%s - %s' % ([""," "][IAMDEBUGGING == True],datetime.now().strftime('%H:%M:%S'),mssg))
  else:
    appendLOG(u'%s' % (mssg))

def printDEBUG(mssg,newline=True):
  if IAMDEBUGGING == True:
    if newline==True:
      appendLOG(u'\n[%s - %s]' % (datetime.now().strftime('%H:%M:%S'),mssg))
    else:
      appendLOG(u'[%s]' % (mssg))

def appendLOG(strMSG):

    try:
        logDATETIME = datetime.now()
        strDATE = '%s%s%s' % ((str(logDATETIME.year)).zfill(4), (str(logDATETIME.month)).zfill(2), (str(logDATETIME.day)).zfill(2))

        objfileLIST = open(WHERE_IAM + "/log/" + strDATE,"a")
        objfileLIST.write(strMSG)
        objfileLIST.close()

        return True

    except:
        print("Error al guardar en el log")
        return False

def formatDATE(strDATE):
  if strDATE:
    strDAY = int(datetime.strptime(strDATE, "%Y-%m-%d").strftime("%d"))
    strMONTH = int(datetime.strptime(strDATE, "%Y-%m-%d").strftime("%m"))
    strYEAR = int(datetime.strptime(strDATE, "%Y-%m-%d").strftime("%Y"))

    if(strMONTH == 1):
      strMONTH = "Ene"
    elif(strMONTH == 2):
      strMONTH = "Feb"
    elif(strMONTH == 3):
      strMONTH = "Mar"
    elif(strMONTH == 4):
      strMONTH = "Abr"
    elif(strMONTH == 5):
      strMONTH = "May"
    elif(strMONTH == 6):
      strMONTH = "Jun"
    elif(strMONTH == 7):
      strMONTH = "Jul"
    elif(strMONTH == 8):
      strMONTH = "Ago"
    elif(strMONTH == 9):
      strMONTH = "Sep"
    elif(strMONTH == 10):
      strMONTH = "Oct"
    elif(strMONTH == 11):
      strMONTH = "Nov"
    elif(strMONTH == 12):
      strMONTH = "Dic"

    return "%d %s %d" % (strDAY, strMONTH, strYEAR)
  else:
    return ""

def formatDNI(strDNI):
  if strDNI:
    return "{:,}".format(int(strDNI)).replace(",",".")
  else:
    return ""


def stopWaitCOMMData():
  global waitingCOMMData
  waitingCOMMData = False

def setBacklight(action=None):
  fileBrightness = open( "/sys/class/backlight/backlight.11/brightness" , "w" )
  if (action == "shutdown") :
    fileBrightness.write("0")
    printDEBUG("Shutting brightness display")
  elif (action == "standby"):
    fileBrightness.write("10")
    printDEBUG("Standing by brightness display")
  elif (action == "powerup"):
    fileBrightness.write("100")
    printDEBUG("Turning on brightness display")
  else:
    pass
  fileBrightness.close

def getCOMMData(serialFile, timeout=None):
  global waitingCOMMData
  global TIMEOUT_BACKLIGHT_TURNOFF
  global TIMEOUT_BACKLIGHT_STANDBY

  try:

    objCOMM = serial.Serial(serialFile, timeout=timeout)
    objCOMM.flushInput()

    if timeout:
      timerOut = Timer(timeout, stopWaitCOMMData)
      timerOut.start()

    Backlighter_turnoff = Timer(TIMEOUT_BACKLIGHT_TURNOFF, setBacklight, ["shutdown"] )
    Backlighter_turnoff.start()

    Backlighter_standby = Timer(TIMEOUT_BACKLIGHT_STANDBY, setBacklight, ["standby"] )
    Backlighter_standby.start()


    waitingCOMMData = True
    strBuffer = ""
    printDEBUG("Esperando COMMData...")

    while waitingCOMMData:
      bytesToRead = objCOMM.inWaiting()
      if timeout: objCOMM.timeout = timeout
      if bytesToRead > 0: strBuffer += objCOMM.read(bytesToRead)
      if chr(0x0d) in strBuffer: break

    printDEBUG( "COMMData: [%s]" % ( strBuffer ) )

    setBacklight("powerup")

    if objCOMM.isOpen(): objCOMM.close()

    if timeout: timerOut.cancel()
    Backlighter_turnoff.cancel()
    Backlighter_standby.cancel()


    if strBuffer:
      strBuffer = strBuffer.replace(chr(0x0a),'')
      strBuffer = strBuffer.replace(chr(0x0d),'')
      printDEBUG( "COMMData: [%s]" % ( strBuffer ) )
      #return strBuffer.decode().encode('utf-8')
      printDEBUG( "ENDCOMMData" )
      return strBuffer

    else:
      return CONST_NOCREDENTIALS

  except Exception,e:
    printOUT("EXCEPT: " + str(e))
    printOUT(traceback.print_exc(file=sys.stdout))
    if strBuffer:
      return strBuffer
    else:
      return None


def restart_program():
    """Restarts the current program.
    Note: this function does not return. Any cleanup action (like
    saving data) must be done before calling this function."""
    python = sys.executable
    os.execl(python, python, * sys.argv)

