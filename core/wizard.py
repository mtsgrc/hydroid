#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4.QtCore import QTimer
from PyQt4.QtGui import QApplication, QMessageBox
from ui_main import *
from global_func import *
from mainXML import *
from mainWEBSYS import *
from class_main import *
import sys, traceback, os
import signal
import time
import glob
import threading
import os
from sys import exit
from os import path

reload(sys)
sys.setdefaultencoding('utf8')

# Variable
__pyqt_app = None

# INTERRUPCION CTRL+C
def sigint_handler(*args):
  sys.stderr.write('\r')
  printOUT("Cerrando aplicacion por interrupcion Ctrl+C")
  exit()

def daemon_restart_if_file_rst_exists():
  rootdir=os.path.dirname(os.path.realpath(__file__))
  fexists=os.path.isfile(rootdir + "/rst")
  if fexists:
    os.remove(rootdir + "/rst")
    restart_program()
  threading.Timer(5,daemon_restart_if_file_rst_exists).start()


# ------------------------------------------------------------- MAIN
if __name__ == "__main__":
  printOUT("================ [ Inicio de Hydroid ] ================")
  signal.signal(signal.SIGINT, sigint_handler)
  nroSession = 0

  # ----------------------------------------------------------- CHECK DIRS
  if not os.path.exists(DB_DIR): os.makedirs(DB_DIR)
  if not os.path.exists(DBIMGS_DIR): os.makedirs(DBIMGS_DIR)

  daemon_restart_if_file_rst_exists()

  try:

    # --------------------------------------------------------- CREAMOS APLICACION Y VENTANA PRINCIPAL
    app = QtGui.QApplication(sys.argv)
    app.setStyle("GTK+")

    # --------------------------------------------------------- LOOP INFINITO
    while True:

      nroSession += 1
      printOUT(" ")
      printOUT("------ Sesion %d ------" % (nroSession))

      # ------------------------------------------------------- MUESTRA DE PANTALLA
      mainWinAPP = MainWizard()
      mainWinAPP.show()

      # ------------------------------------------------------- CREACION DE OBJETOS
      objStepRFID = None
      objStepScannerBarcode = None
      objStepCAM = None
      objStepEND = None

      objBarcode = None
      objActualUser = None
      objActualUser = Usuario()
      objCONFIGAPP = None
      devsSTATS = None

      # ------------------------------------------------------- DETECCION DE DISPOSITIVOS
      if ( ( FILE_RFIDMOTOR == None ) or ( FILE_IDREADER == None ) or (FILE_CAMERA == None ) ) :
        mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Detectando dispositivos...</font></body></html>",2.0)
        objDACDs = CONFIGDEVS()
        objDACDs.detectDevs()

        if ( ( objDACDs.getRFIDMOTOR() != None ) and ( objDACDs.getCAMERA() != None ) and ( objDACDs.getIDREADER() != None ) ) :
          mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Finalizado</font></body></html>",2.0)
          mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Reiniciando sistema...</font></body></html>",2.0)
          restart_program()

        else:
          arrNotDetected = objDACDs.getNotDetected()
          if arrNotDetected:
            strNotDetected = '<br />'.join(arrNotDetected)
          else:
            strNotDetected = ""
          mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Dispositivos no detectados</font><br /><font style='font-size: 14pt;'>"+strNotDetected+"</font></body></html>",20.0)
          continue
      # ------------------------------------------------------- CHECKEO DE PRESENCIA DE DISP


      if ( ( CFG_TERMINALNAME == None) and ( CFG_TERMINALID == None) ):
        mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Bienvenido a Taker</font></body></html>",3.0)
      else:
        mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Bienvenido a Taker</font><br /><font style='font-size: 14pt;'>Terminal \""+CFG_TERMINALNAME+"\"</font></body></html>",3.0)


      devsDISCNN = []

      if not os.path.exists(FILE_RFIDMOTOR):
        printOUT("RFIDMotor not connected")
        devsDISCNN.append('Motor RFID')
      if not os.path.exists(FILE_IDREADER):
        printOUT("IDReader not connected")
        devsDISCNN.append('Lector ID')
      if not os.path.exists(FILE_CAMERA):
        printOUT("Camera not connected")
        devsDISCNN.append('Camara')

      if devsDISCNN:
        strDevsDISCNN = ', '.join(devsDISCNN)
        mainWinAPP.goToStep("widgetMSSG","Dispositivos desconectados: \n %s" % (strDevsDISCNN))
        time.sleep(20)

        printOUT("Fin Sesion %d \n" % (nroSession))
        continue

      # ------------------------------------------------------- INICIO SECUENCIA / PASOS
      if ( ( CFG_TERMINALNAME == None) or ( CFG_TERMINALID == None) ):
        printDEBUG("Sistema no configurado... Solicitando configuracion")
        mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Configurando sistema...</font><br /><font style='font-size: 14pt;'>Por favor ingrese configuración</font></body></html>")
      else:
        mainWinAPP.goToStep("widget1")                                        # ----------------------------- PASO 1 - barcode 2d ------------------------------

      objStepScannerBarcode = StepScannerBarcode()
      objStepScannerBarcode.waitBarcode()                                     # Espera ingreso de Barcode

      printDEBUG( "STRING: [" + objStepScannerBarcode.getDataString() + "]" )

      objBarcode = objStepScannerBarcode.getUserObject()                      # Devuelve objeto tipo PDF, o Code39 con datos

      if objBarcode != None:

        printOUT("Ok.", False)
        if isinstance(objBarcode, CONFIGAPP):

          if objBarcode.getResetFactory() == True:
            objBarcode.factoryCFGfile()
            mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Reiniciando sistema a fábrica</font></body></html>",3.0)
            restart_program()
          if objBarcode.getResetSoftware() == True:
            mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Reiniciando sistema...</font></body></html>",3.0)
            restart_program()


          objBarcode.changeCFGfile()
          printDEBUG("Usuario ingreso configuracion de sistema")
          idTerminal = objBarcode.getID()
          nameTerminal = objBarcode.getNAME()
          mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Nueva configuración</font><br /><font style='font-size: 15pt;'>"+str(nameTerminal)+" ("+str(idTerminal)+")</font></body></html>",3.0)
          mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 16pt;'>Reiniciando sistema...</font></body></html>",3.0)
          restart_program()

        if objBarcode == "testAppOn":
          IAMTESTING = True
          mainWinAPP.goToStep("widgetMSSG","MODO APP: Testing")
          continue
        elif objBarcode == "testAppOff":
          IAMTESTING = False
          mainWinAPP.goToStep("widgetMSSG","MODO APP: Normal")
          continue
        else:
          mainWinAPP.goToStep("widgetMSSG","Retire Identificación")
          if IAMTESTING == False:
            mainWinAPP.goToStep("widgetMSSG","Espere por favor...")
          else:
            mainWinAPP.goToStep("widgetMSSG","Modo testing Activado")


        objWebSys = WebSysSender()                                              # Objeto que consulta web para habilitar expendio de tarjeta
        uAbleCard = "false"

        if isinstance(objBarcode, barcodePDF):
          printDEBUG("Usuario ingreso identificacion de tipo DNI")
          objActualUser.setDNIData(objBarcode.pdfDNI, objBarcode.pdfApellidos, objBarcode.pdfNombres, objBarcode.pdfFechaNac)
          if IAMTESTING != True:
            uAbleCard = objWebSys.checkDNI(objBarcode.pdfDNI)

        if isinstance(objBarcode, barcodeC39):
          printDEBUG("Usuario ingreso identificacion de tipo CARNET")
          objActualUser.setIDNData(objBarcode.idNUser)
          if IAMTESTING != True:
            uAbleCard = objWebSys.checkIDN(objBarcode.idNUser)

        if IAMTESTING != True:
          if not uAbleCard:
            mainWinAPP.goToStep("widgetMSSG","Falló Conexión a Servidor")
            mainWinAPP.goToStep("widgetMSSG","Por favor intente luego")
            printOUT("Fin Sesion %d \n" % (nroSession))                          # Finalizamos
            continue

        if uAbleCard != RETXML_GIVECARD and IAMTESTING == False :

          if RETXML_IDNNOEXIST in uAbleCard:
            mainWinAPP.goToStep("widgetMSSG","Usuario inexistente")
            mainWinAPP.goToStep("widgetMSSG","Por favor intente luego")
            printOUT("Fin Sesion %d \n" % (nroSession))                          # Finalizamos
            continue

          if RETXML_UPENDIENTE in uAbleCard:
            mainWinAPP.goToStep("widgetMSSG","<html><head></head><body><font style='font-size: 14pt;'>Comunicarse con Taker para habilitación definitiva de credencial</font><br /><font style='font-size: 16pt;'>Tel: 0810-666-7956<br />www.taker.com.ar</font></body></html>",10.0)
            mainWinAPP.goToStep("widgetMSSG","Por favor intente luego")
            printOUT("Fin Sesion %d \n" % (nroSession))                          # Finalizamos
            continue

          if RETXML_MAXCARDS in uAbleCard:
            mainWinAPP.goToStep("widgetMSSG","Credencial ya retirada")
            printOUT("Fin Sesion %d \n" % (nroSession))                          # Finalizamos
            continue

        else:
          pass

        mainWinAPP.goToStep("widget2")                                      # ----------------------------- PASO 2 - FOTO ----------------------------------

        objStepCAM = StepCAM(mainWinAPP)                                                      # Creamos objeto que tomara foto
        objStepCAM.streamAndCapture()

        if objStepCAM.objImage:

          printOUT("Ok.", False)
          objActualUser.setImgPhoto(objStepCAM.objCAMERA.snapshot)                            # Seteamos Imagen en Usuario

          mainWinAPP.goToStep("widget3")                                    # ----------------------------- PASO 3 - DATOS ------------------------------------

          objStepEND = StepEND()                                                              # Objeto que mostrara datos al usuario durante 5 segundos
          objStepEND.showDataInWidget(mainWinAPP,objActualUser,objStepCAM.objCAMERA.snapshot)
          time.sleep(5)

          printOUT("Ok.", False)

          objStepRFID = StepRFID()                                                            # Objeto que escucha RFID
          objStepRFID.listenRFID()                                                            # Escuchamos RFID

          if objStepRFID.actRFID == CONST_NOCREDENTIALS:
            mainWinAPP.goToStep("widgetMSSG","No hay credenciales disponibles")
            mainWinAPP.goToStep("widgetMSSG","Por favor intente luego")
            continue


          if len(objStepRFID.actRFID) == 10:                                       # Validacion de tarjeta detectada

            objActualUser.setRFID(objStepRFID.actRFID)                                          # Seteamos RFID detectado al obj Usuario actual

            # --- Persistencia Usuario actual ----

            objXMLUser = xmlData()
            objXMLUser.setXMLfromUSEROBJ(objActualUser)

            printOUT(objXMLUser.getXMLstring())

            synced = False

            if IAMTESTING != True:

              # --- Envio de datos al servidor ---
              if isinstance(objBarcode, barcodePDF):
                printOUT("Setting DNI User to send Server .")
                objWebSys.setDNI(objXMLUser.getXMLstring())
                retHost = objWebSys.sendDNI()

              if isinstance(objBarcode, barcodeC39):
                printOUT("Setting IDN User to send Server .")
                objWebSys.setIDN(objXMLUser.getXMLstring())
                retHost = objWebSys.sendIDN()

              if retHost:
                printDEBUG("Respuesta Host: %s" % (retHost) )
                if retHost.strip().isdigit():
                  synced = True
              else:
                mainWinAPP.goToStep("widgetMSSG","Falló Conexión a Servidor")

              # Guardamos xml con un "_" antes del nombre del fichero si se envio datos al servidor
              objXMLUser.saveXML(synced)

            # PASO DE MOSTRAR "RETIRE TARJETA"
            printOUT("Ok.", False)
            mainWinAPP.goToStep("widget4")                                                     # Mostramos retire tarjeta
            time.sleep(10)

          else:                                                       # Datos de lector RFID invalidos
            printOUT("ERROR: No se reconocio RFID", False)
            mainWinAPP.goToStep("widgetMSSG","Error interno de tarjetas")
            mainWinAPP.goToStep("widgetMSSG","Por favor intente luego")
            time.sleep(5)
        else:                                                       # No se pudo capturar foto
          printOUT("ERROR: No se pudo capturar foto", False)
          mainWinAPP.goToStep("widgetMSSG","Error en la cámara")
          mainWinAPP.goToStep("widgetMSSG","Por favor intente luego")
          time.sleep(5)
      else:                                                       # Datos de scanner invalidos (PDF417) - DNI
        printOUT("ERROR: No se reconocio barcode", False)
        mainWinAPP.goToStep("widgetMSSG","Error de lectura de identificación")
        time.sleep(5)

      printOUT("Fin Sesion %d \n" % (nroSession))                          # Finalizamos

    sys.exit(app.exec_())

  except Exception,e:
    printOUT("Error ocurrido: " + str(e))
    traceback.print_exc(file=sys.stdout)
    exit()
