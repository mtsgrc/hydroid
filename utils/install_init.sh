#!/usr/bin/env bash
ACTUALDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARENTDIR="$(dirname "$ACTUALDIR")"

# Copiamos fichero que sincroniza hora bajo directorio init.d y actualizas app al inicio
chmod 755 $ACTUALDIR/keepalive.sh
cp $ACTUALDIR/keepalive.sh /etc/init.d/hydroid_keepalive.sh
update-rc.d hydroid_keepalive.sh defaults

# Habilitar network bajo eth0
sed -i '/iface eth0 inet dhcp/c\auto eth0\niface eth0 inet dhcp' /etc/network/interfaces
