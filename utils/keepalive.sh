#!/usr/bin/env bash

## BEGIN INIT INFO
# Provides:          hydroid_keepalive
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start utils for hydroid
# Description:       None description
### END INIT INFO

/etc/init.d/ntp stop
ntpdate -s ar.pool.ntp.org
/etc/init.d/ntp start

hwclock --systohc
