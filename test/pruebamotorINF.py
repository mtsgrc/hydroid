import serial
import time
from threading import Timer
import os
from os.path import join

idVendor = "04d8"
idProduct = "00df"
isWaiting = False
defRotorTime = 3.0
ttyRotorRFID = "/dev/ttyACM0"

def cfgMotor():
 try:
  comS = serial.Serial(ttyRotorRFID, 9600, timeout=3)
  while True:
   dataI = raw_input("\n\n 1) Desea escribir un valor para el tiempo (ej: 35 es 3,5 seg) y/o confirme/salte con <ENTER>: ")
   if not dataI:
    return
   print "Configurando " + str(int(dataI) / 10.0) + " segundos... " ,
   if dataI.isdigit():
    if int(dataI) > 0:
     dataI = dataI.zfill(2)
     comS.write('c'+dataI)
     print "OK."
     defRotorTime = int(dataI) / 10.0
     break
    else:
     print "ERROR.\n"
     print "Por favor ingrese un valor mayor que 0"
   else:
    print "ERROR.\n"
    print "Por favor ingrese un valor numerico"
 except KeyboardInterrupt:
  print "\n"
  exit()
 except Exception as exc:
  print str(exc)

def expendCARD_infinite():
 try:
  comS = serial.Serial(ttyRotorRFID, 9600, timeout=defRotorTime)
  while True:
   expendCARD(comS)
   #tiempodemora = cronometro(expendCARD)(comS)
   #print "(Tiempo demora: %s)" % (str(tiempodemora))

 except KeyboardInterrupt:
  print "\n"
  exit()
 except Exception as exc:
  print str(exc)

def stopWait():
 global isWaiting
 isWaiting = False

def expendCARD(comS):
 try:
  global isWaiting
  #dataI = raw_input("Presione <ENTER> para expulsar una tarjeta...")
  print "Expulsando tarjeta...\n"
  comS.write('s')

#  timerOut = Timer(10.0, stopWait)
  isWaiting = True
  while isWaiting:
   arrData = []
   comS.flushInput()
   xLine = comS.readline()
   print ">%s" % (xLine)
   if xLine:
    for xChar in xLine:
     if ord(xChar):
      arrData.append(xChar)

    if arrData:
     print ">>>>>>>>>>>> ID tarjeta:" + ''.join(arrData)
     comS.write('p')
     isWaiting = False

  #timerOut.cancel()
  time.sleep(5)
  print "\n\n"

 except KeyboardInterrupt:
  print "\n"
  exit()
 except Exception as exc:
  print str(exc)

def cronometro(funcion):
 def funcion_a_ejecutar(*argumentos):
  inicio = time.time()
  ret = funcion(*argumentos)
  fin = time.time()
  tiempo_total = fin - inicio
  return tiempo_total
 return funcion_a_ejecutar


def find_tty_usb(idVendor, idProduct):
 """find_tty_usb('067b', '2302') -> '/dev/ttyUSB0'"""
 print ("Searching for: idVendor > %s & idProduct > %s " % (idVendor, idProduct)) ,
 # Note: if searching for a lot of pairs, it would be much faster to search
 # for the enitre lot at once instead of going over all the usb devices
 # each time.
 for dnbase in os.listdir('/sys/bus/usb/devices'):
  dn = join('/sys/bus/usb/devices', dnbase)
  if not os.path.exists(join(dn, 'idVendor')):
   continue
  idv = open(join(dn, 'idVendor')).read().strip()
  if idv != idVendor:
   continue
  idp = open(join(dn, 'idProduct')).read().strip()
  if idp != idProduct:
   continue
  for subdir in os.listdir(dn):
   if subdir.startswith(dnbase+':'):
    for subsubdir in os.listdir(join(dn, subdir)):
     if subsubdir.startswith('ttyUSB'):
      print ( " ====> %s" % (join('/dev', subsubdir)) )
      return join('/dev', subsubdir)

print "\n+===========================================+"
print "| Probador de la expendedora de tarjetas... |"
print "+===========================================+"
#ttyRotorRFID = find_tty_usb(idVendor, idProduct)
if ttyRotorRFID:
# cfgMotor()
 expendCARD_infinite()
else:
 print "\nNo se ha detectado la controladora de la expendedora"
print "Cerrando aplicacion...\n"
