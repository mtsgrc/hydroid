import sys
import os
from os.path import join

def find_tty_usb(idVendor, idProduct):
    """find_tty_usb('067b', '2302') -> '/dev/ttyUSB0'"""
    print "Searching for: idVendor > %s & idProduct > %s" % (idVendor, idProduct)
    # Note: if searching for a lot of pairs, it would be much faster to search
    # for the enitre lot at once instead of going over all the usb devices
    # each time.
    for dnbase in os.listdir('/sys/bus/usb/devices'):
        dn = join('/sys/bus/usb/devices', dnbase)
        if not os.path.exists(join(dn, 'idVendor')):
            continue
        idv = open(join(dn, 'idVendor')).read().strip()
        if idv != idVendor:
            continue
        idp = open(join(dn, 'idProduct')).read().strip()
        if idp != idProduct:
            continue
        for subdir in os.listdir(dn):
            if subdir.startswith(dnbase+':'):
                for subsubdir in os.listdir(join(dn, subdir)):
                    print "<%s>" % (subsubdir)
                    if subsubdir.startswith('ttyUSB'):
                        return join('/dev', subsubdir)

print ""
if len(sys.argv) < 2:
  sys.exit('Usage: %s idVendor:idProduct' % sys.argv[0])
else:
  tmpArg = sys.argv[1]
  idVendor = tmpArg.split(':')[0]
  idProduct = tmpArg.split(':')[1]
  strtty = find_tty_usb(idVendor, idProduct)
  if strtty:
    print " >>> Dev founded on %s" % (strtty)
  else:
    print "NOT FOUNDED!!"
print ""






