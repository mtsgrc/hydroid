#!/usr/bin/env python
# -*- coding: utf-8 -*-
#


import serial
from serial import SerialException, SerialTimeoutException

#Modified code from main loop: 
s = serial.Serial("/dev/serial/by-id/usb-Honeywell_Imaging___Mobility_1400g_15014B0A48-if00")
s.flushInput()
strBuffer = ""
#Modified code from thread reading the serial port
while 1:
  bytesToRead = s.inWaiting()
  if bytesToRead > 0: strBuffer += s.read(bytesToRead)
  if strBuffer:
    print "{"+strBuffer+"}"
#  print ( "%d" % len(strBuffer) )
  if chr(0x0d) in strBuffer: break

if strBuffer:
  strBuffer = strBuffer.replace(chr(0x0a),'')
  strBuffer = strBuffer.replace(chr(0x0d),'')
  print( "COMMData: [%s]" % ( strBuffer ) )
  #strBuffer.decode().encode('utf-8')

