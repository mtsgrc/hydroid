import sys
import os
import re
import traceback
from curses.ascii import *
from os.path import join
from serial import Serial

CONSTBCS_BUFFERLEN = 8
CONSTBCS_SUFFIX = chr(015) + chr(012)                         # {0x0f}{0x0c}

CONSTBCS_PDF417CODE = chr(130)                                # 'X'
CONSTBCS_CODE39CODE = chr(102)                                # 'B'
CONSTBCS_QRCODE = chr(120) + chr(060) + chr(061)              # 'P01'
#067b:2303		lector RFID
#05f9:4204		scanner barcode

CONST_SCANNER_IDVENDOR = "05f9"	
CONST_SCANNER_IDPRODUCT = "4204"

def getDataChile(strFull, typeBC):
  try:
    dataCHLPDF = {"RUT": '', "Apellido": ''}
    if typeBC == "pdfchl1":
        dataCHLPDF["RUT"] = strFull[0:9]
        posAct = 0
        posSpace = 0
        posNotPrintable = 0
        for xChar in strFull:
          posAct += 1
          if xChar == ' ':
            print "Space > %s" % ( str( posAct  ) )
            posSpace = posAct
          if not isprint(xChar):
            print "NotPrintable > %s" % ( str( posAct ) )
            posNotPrintable = posAct - 22
            break
        print "[%s]" % (strFull[posSpace:posNotPrintable])
        dataCHLPDF["Apellido"] = strFull[posSpace:posNotPrintable]

    if typeBC == "pdfchl2":
        posRUN = 0
        posRUN = strFull.find("RUN=")
        print "POSDATA: %s" % ( str(posRUN) )
        if posRUN > 0:
          dataCHLPDF["RUT"] = strFull[posRUN+4:posRUN+14]
    return str(dataCHLPDF)
  except Exception, err:
    print traceback.format_exc()

def getTypeBarcode(strFull):
    if re.compile('^@\d{8,} *@[A-Z]{1}@\d{1}').findall(strFull.strip()):
        return "pdfarg1"
    if re.compile('^\d{11}@[A-Z ]*@[A-Z ]*@[M|F]{1}@\d{8,}@').findall(strFull.strip()):
        return "pdfarg2"
    if re.compile('^\d+$').findall(strFull.strip()):
        return "oldcarnet"
    if re.compile('^\d{8}[0-9|K]{1}\d{9} ').findall(strFull.strip()):
        return "pdfchl1"
    if re.compile('^https:\/\/').findall(strFull.strip()):
        return "pdfchl2"
    return "unknown"

def listenBarcode(ttyUSBfile):
  try:
    print "listenning Barcode on: %s" % ( ttyUSBfile )
    serCOM = Serial(ttyUSBfile, 9600, timeout=1)

    while True:

      lineScan = serCOM.readline()
      arrLineScan = []

      if lineScan:
        for xChar in lineScan:
            if ord(xChar):
              arrLineScan.append(xChar)

        if arrLineScan:
          print "  Type: %s" % ( getTypeBarcode(''.join(arrLineScan)) )
          print " Array: %s len: %s" % ( arrLineScan, str(len(arrLineScan)) )
          print "  Text: %s" % ( ''.join(arrLineScan) )
          if getTypeBarcode(''.join(arrLineScan))[:6] == "pdfchl":
            print "  Data: %s" % ( getDataChile(''.join(arrLineScan), getTypeBarcode(''.join(arrLineScan))) )
          print "\n"

  except KeyboardInterrupt:
    print "Saliendo por interrupcion...\n"

  except IOError as ioerr:
    print "Se perdio conexion con dispositivo..."

  except Exception as excp:
    print "Error: %s\n" % (str(excp))

def find_tty_usb(idVendor, idProduct):
    """find_tty_usb('067b', '2302') -> '/dev/ttyUSB0'"""
    print "Searching for: idVendor > %s & idProduct > %s" % (idVendor, idProduct)
    # Note: if searching for a lot of pairs, it would be much faster to search
    # for the enitre lot at once instead of going over all the usb devices
    # each time.
    for dnbase in os.listdir('/sys/bus/usb/devices'):
        dn = join('/sys/bus/usb/devices', dnbase)
        if not os.path.exists(join(dn, 'idVendor')):
            continue
        idv = open(join(dn, 'idVendor')).read().strip()
        if idv != idVendor:
            continue
        idp = open(join(dn, 'idProduct')).read().strip()
        if idp != idProduct:
            continue
        for subdir in os.listdir(dn):
            if subdir.startswith(dnbase+':'):
                for subsubdir in os.listdir(join(dn, subdir)):
                    if subsubdir.startswith('ttyUSB'):
                        return join('/dev', subsubdir)

listenBarcode(find_tty_usb(CONST_SCANNER_IDVENDOR, CONST_SCANNER_IDPRODUCT))

