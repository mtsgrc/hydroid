import serial

def cfgMotor():
 global isWaiting
 try:
  comS = serial.Serial('/dev/ttyUSB0', 9600, timeout=3)
  dataI = raw_input("Escriba un valor para el tiempo (ej: 35 es 3,5 seg): ")
  comS.write('s'+dataI)
  print str(int(dataI) / 10.0) + "s configurados para el motor"

 except KeyboardInterrupt:
  print "\n"
  exit()
 except Exception as exc:
  print str(exc)


print "Configurador de tiempo del Motor...\n"
cfgMotor()
print "Cerrando aplicacion...\n"
