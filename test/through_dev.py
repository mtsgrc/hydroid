import sys
from curses.ascii import *

CONSTBCS_BUFFERLEN = 8
CONSTBCS_SUFFIX = chr(0x0a) + chr(0x0d)                       # {0x0a}{0x0d}

CONSTBCS_PDF417CODE = chr(130)                                # 'X'
CONSTBCS_CODE39CODE = chr(102)                                # 'B'
CONSTBCS_QRCODE = chr(120) + chr(060) + chr(061)              # 'P01'


def listenHIDRAW(hidfile='/dev/hidraw0'):
  try:

    fp_hid = open(hidfile, 'rb')

    while True:
      arrData = []

      while True:

        bufferScan = fp_hid.read(CONSTBCS_BUFFERLEN)
        #bufferScan = fp_hid.readline()
        #print bufferScan

        if bufferScan:
          for xByte in bufferScan:
            if ord(xByte):
              if isprint(xByte):
                #if xByte == 0x0a or xByte == 0x0d:
                arrData.append(xByte)

        if arrData:
          break

        #if CONSTBCS_SUFFIX in ''.join(arrData):
        #  break

      print "%s len: %s\n" % (arrData, len(arrData))
      print "[%s] len: %s\n" % (repr(''.join(arrData)), len(''.join(arrData)))

  except KeyboardInterrupt:
    print "Saliendo por interrupcion...\n"

  except IOError as ioerr:
    print "Se perdio conexion con dispositivo..."

  except Exception as excp:
    print "Error: %s\n" % (str(excp))
listenHIDRAW()
