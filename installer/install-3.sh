#!/bin/bash

echo " > Managing users... Adding, Setting Own folder, Bash terminal"
delay 3

useradd hydroid
passwd hydroid
mkdir /home/hydroid
chown hydroid:hydroid -R /home/hydroid
chmod 755 -R /home/hydroid
usermod -d /home/hydroid hydroid
sed  -i  's/hydroid:x:1001:1003::\/home\/hydroid:\/bin\/sh/hydroid:x:1001:1003::\/home\/hydroid:\/bin\/bash/' /etc/passwd
delay 1
echo "hydroid    ALL=(ALL) ALL" >> /etc/sudoers
echo " - New user:password added [hydroid:medusa]"
delay 10

echo " > Now booting, enter with the new user..."
delay 3
reboot
