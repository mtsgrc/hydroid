#!/bin/bash

echo " > Configuring time"
sleep 3
echo " - Configuring zonetime > Argentina Mendoza"

cp /usr/share/zoneinfo/America/Argentina/Mendoza /etc/localtime

echo " - Zonetime configured"

echo " > Installing NTP for sync time"
sleep 2

apt-get install ntp ntpdate

echo " - NTP client installed"

sleep 3

echo " - Syncing time"
/usr/sbin/ntpdate -u ar.pool.ntp.org

echo " - Done!"

echo " - Setting actual time to RTC"
sudo hwclock --systohc

echo " - Done!"
