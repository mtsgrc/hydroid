#!/bin/bash

echo " > Installing packages for python"
sleep 3
apt-get install python python-pip python-dev
apt-get install python-setuptools python-pygame
apt-get install libtiff4-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.5-dev tk8.5-dev
apt-get install pyqt4-dev-tools python python-qt4 qt4-designer
apt-get install python-tk libgnomeui-0 libgnomeui-dev python-gnome2
sleep 1
echo " - Python installed"

echo " > "
echo " - "
