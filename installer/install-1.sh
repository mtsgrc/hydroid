#!/bin/bash

echo " > Updating kernel..."
delay 3

cd /opt/scripts/tools
git pull
./update_kernel.sh      # stable
delay 3

echo " - Kernel successfully updated"
echo " - Now rebooting..."
delay 5

reboot
