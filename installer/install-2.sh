#!/bin/bash

echo " > Installing graphics..."
sleep 3
cd /opt/scripts/tools/
git pull
cd /opt/scripts/tools/graphics/
./ti-tilcdc.sh

sleep 1
echo " - Graphics installed"
echo " - Now rebooting"
sleep 3
reboot
