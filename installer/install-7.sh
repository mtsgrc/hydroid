#!/bin/bash

echo " > Installing LXDE"
sleep 3
sudo apt-get install lxde-core
sleep 1

echo " - LXDE installed"
echo " > Installing LIGHTDM"
sleep 3

sudo apt-get install lightdm

sleep 1
echo " - LIGHTDM installed"
